﻿using BeerLab.Services.Test;
using Matrix.DataBase;
using Matrix.Models;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Matrix.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matrix.Services.Test.FollowerService_Tests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task Return_Correct_Async_GetFollower()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_GetFollower));

            var photo1 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvuknx9na",
                IsMain = true
            };

            var photo2 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvtknx9na",
                IsMain = true
            };

            var observer = new User
            {
                UserName = "toma",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = new List<UserActivity>(),
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = new List<UserActivity>(),
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var following = new UserFollowing
            {
                Observer = observer,
                Target = target
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                observer.Photos.Add(photo1);
                target.Photos.Add(photo2);
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                var profileDto = new ProfileDto
                {
                    DisplayName = observer.DisplayName,
                    UserName = observer.UserName,
                    Image = observer.Photos.FirstOrDefault(photo => photo.IsMain)?.Url,
                    Photos = observer.Photos,
                    Bio = observer.Bio,
                    FollowersCount = observer.Followers.Count(),
                    FollowingCount = observer.Followings.Count()
                };

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);
                profileService.Setup(x => x.ReadProfile(observer.UserName)).ReturnsAsync(profileDto);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                var add = sut.Add(target.UserName);

                var result = await sut.Get(target.UserName, "followers");


                Assert.AreEqual(result.Count, 1);
            }
        }

        [TestMethod]
        public async Task Return_Correct_Async_GetFollowing()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_GetFollowing));

            var photo1 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvuknx9na",
                IsMain = true
            };

            var photo2 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvtknx9na",
                IsMain = true
            };

            var observer = new User
            {
                UserName = "toma",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = new List<UserActivity>(),
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = new List<UserActivity>(),
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var following = new UserFollowing
            {
                Observer = target,
                Target = observer
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                observer.Photos.Add(photo1);
                target.Photos.Add(photo2);
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                var profileDto = new ProfileDto
                {
                    DisplayName = observer.DisplayName,
                    UserName = observer.UserName,
                    Image = observer.Photos.FirstOrDefault(photo => photo.IsMain)?.Url,
                    Photos = observer.Photos,
                    Bio = observer.Bio,
                    FollowersCount = observer.Followers.Count(),
                    FollowingCount = observer.Followings.Count()
                };

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);
                profileService.Setup(x => x.ReadProfile(observer.UserName)).ReturnsAsync(profileDto);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                var add = sut.Add(target.UserName);

                var result = await sut.Get(observer.UserName, "following");


                Assert.AreEqual(result.Count, 1);
            }
        }
    }
}
