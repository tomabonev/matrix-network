﻿using BeerLab.Services.Test;
using Matrix.DataBase;
using Matrix.Models;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Matrix.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Matrix.Services.Test.FollowerService_Tests
{
    [TestClass]
    public class Add_Should
    {
        [TestMethod]
        public async Task Return_Correct_Async_AddFollower()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_AddFollower));

            var observer = new User
            {
                UserName = "toma",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var following = new UserFollowing
            {
                Observer = observer,
                Target = target
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                var result = await sut.Add(target.UserName);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Return_Correct_Async_AddFollowerNull()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_AddFollowerNull));

            var observer = new User
            {
                UserName = "toma",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                arrangeContext.Users.Add(observer);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                await Assert.ThrowsExceptionAsync<System.WebException>(async () => await sut.Add(null));
                await Assert.ThrowsExceptionAsync<System.WebException>(async () => await sut.Add("asen"));
            }
        }

        [TestMethod]
        public async Task Return_Correct_Async_AddFollowerFollowed()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_AddFollowerFollowed));

            var observer = new User
            {
                UserName = "toma",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var following = new UserFollowing
            {
                Observer = observer,
                Target = target
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);
                arrangeContext.Followings.Add(following);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                await Assert.ThrowsExceptionAsync<System.WebException>(async () => await sut.Add(target.UserName));
            }
        }

    }
}
