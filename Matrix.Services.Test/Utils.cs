using Matrix.DataBase;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerLab.Services.Test
{
    public class Utils
    {
        public static DbContextOptions<MatrixDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<MatrixDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
