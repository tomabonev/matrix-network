﻿using BeerLab.Services.Test;
using Matrix.DataBase;
using Matrix.Models;
using Matrix.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matrix.Services.Test.PhotoService_Test
{
    [TestClass]
    public class CreatePhoto_Should
    {
        [TestMethod]
        public async Task Return_Correct_Async_CreatePhoto()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_CreatePhoto));

            var photo1 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvuknx9na",
                IsMain = true
            };

            var user = new User
            {
                Id = "81bf453e-cdc6-4e07-8771-e4206f082ae4",
                UserName = "toma",
                DisplayName = "Toma",
                Bio = "asdasdasdasdasdasd",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = null,
                Followers = null
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                user.Photos.Add(photo1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                var result = user.Photos.FirstOrDefault();

                //Assert
                Assert.IsInstanceOfType(result, typeof(Photo));
            }
        }
    }
}
