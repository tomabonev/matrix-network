﻿using BeerLab.Services.Test;
using Matrix.DataBase;
using Matrix.Models;
using Matrix.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matrix.Services.Test.PhotoService_Test
{
    [TestClass]
    public class CreateComment_Should
    {
        [TestMethod]
        public async Task Return_Correct_Async_CreateComment()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_CreateComment));

            var photo1 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvuknx9na",
                IsMain = true
            };

            var photo2 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605807368/fhb5lnpdzshtxx2axkod.jpg",
                Id = "fhb5lnpdzshtxx2axkod",
                IsMain = false
            };

            var user = new User
            {
                Id = "81bf453e-cdc6-4e07-8771-e4206f082ae4",
                UserName = "toma",
                DisplayName = "Toma",
                Bio = "asdasdasdasdasdasd",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var activity = new Activity
            {
                Id = new Guid("03a15629-2f40-4db2-ac7c-b335f3fdf4c3"),
                Title = "Past Activity 1",
                Description = "Activity 2 months ago",
                Category = "drinks",
                Date = DateTime.Now.AddMonths(-2),
                City = "London",
                Venue = "Pub",
                IsDeleted = false,
                Comments = new List<Comment>(),
                UserActivities = new List<UserActivity>()
            };

            var comment = new Comment
            {
                Author = user,
                Activity = activity,
                Body = "aaaaaaaaaaaaaaaaa",
                CreatedAt = DateTime.UtcNow
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                activity.Comments.Add(comment);
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                var result = activity.Comments.FirstOrDefault();

                //Assert
                Assert.IsInstanceOfType(result, typeof(Comment));
            }
        }
    }
}
