﻿using Matrix.Services.Contracts;
using Matrix.Web.ApiControllers;
using Moq;
using NUnit.Framework;
using System;

namespace Matrix.Services.Test.Controllers
{
    [TestFixture]
    public class FollowerController_Should
    {
        private Mock<IFollowerService> mockedFollowerService;
        private FollowerController followerController;

        [SetUp]
        public void FollowerControllerShouldSetUp()
        {
            mockedFollowerService = new Mock<IFollowerService>();
            followerController = new FollowerController(mockedFollowerService.Object);
        }

        [Test]
        public void TestConstructor()
        {
            Assert.That(followerController, Is.TypeOf<FollowerController>());
            Assert.IsNotNull(followerController);
        }

        [Test]
        public void ThrowsArgumentNullExceptionWhenFollowerServiceIsNull()
        {
            mockedFollowerService = null;
            Assert.Throws<NullReferenceException>(() => new FollowerController(mockedFollowerService.Object));
        }
    }
}