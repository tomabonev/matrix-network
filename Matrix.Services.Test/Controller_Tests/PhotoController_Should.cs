﻿using Matrix.Services.Contracts;
using Matrix.Web.ApiControllers;
using Moq;
using NUnit.Framework;
using System;

namespace Matrix.Services.Test.Controller_Tests
{
    [TestFixture]
    public class PhotoController_Should
    {
        private Mock<IPhotoService> mockedPhotoService;
        private PhotoController photoController;

        [SetUp]
        public void PhotoControllerShouldSetUp()
        {
            mockedPhotoService = new Mock<IPhotoService>();
            photoController = new PhotoController(mockedPhotoService.Object);
        }

        [Test]
        public void TestConstructor()
        {
            Assert.That(photoController, Is.TypeOf<PhotoController>());
            Assert.IsNotNull(photoController);
        }

        [Test]
        public void ThrowsArgumentNullExceptionWhenPhotoServiceIsNull()
        {
            mockedPhotoService = null;
            Assert.Throws<NullReferenceException>(() => new PhotoController(mockedPhotoService.Object));
        }
    }
}