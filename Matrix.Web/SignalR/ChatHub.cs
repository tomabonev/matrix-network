﻿using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Matrix.Web.SignalR
{
    public class ChatHub : Hub
    {
        #region Fields

        private readonly ICommentService _commentService;
        private readonly IUserService _userService;

        #endregion

        #region Constructors

        public ChatHub(ICommentService commentService, IUserService userService)
        {
            _commentService = commentService;
            _userService = userService;
        }

        #endregion

        #region Public Methods

        public async Task SendComment(CommentDto commentDto)
        {
            UserDto user = await GetUsername();

            commentDto.UserName = user.UserName;

            var commentToReturn = await _commentService.Create(commentDto);

            await Clients.Group(commentDto.ActivityId.ToString()).SendAsync("ReceiveComment", commentToReturn);
        }

        private async Task<UserDto> GetUsername()
        {
            return await this._userService.GetCurrentUser();
        }

        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

            var user = await GetUsername();

            await Clients.Group(groupName).SendAsync("Send", $"{user.DisplayName} has joined the group");
        }

        public async Task RemoveFromGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);

            var user = await GetUsername();

            await Clients.Group(groupName).SendAsync("Send", $"{user.DisplayName} has left the group");
        }

        #endregion
    }
}