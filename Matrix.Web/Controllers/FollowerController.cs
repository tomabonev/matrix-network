﻿using Matrix.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Matrix.Web.ApiControllers
{
    [Route("api/profile")]
    [ApiController]
    public class FollowerController : ControllerBase
    {
        #region Fields

        private readonly IFollowerService _followerService;

        #endregion

        #region Constructors

        public FollowerController(IFollowerService followerService)
        {
            _followerService = followerService;
        }

        #endregion

        #region Public Methods

        [HttpPost("{username}/follow")]
        public async Task<IActionResult> Follow(string username)
        {
            var isFollowed = await _followerService.Add(username);

            return Ok(isFollowed);
        }

        [HttpDelete("{username}/follow")]
        public async Task<IActionResult> Unfollow(string username)
        {
            var isUnFollowed = await _followerService.Delete(username);

            return Ok(isUnFollowed);
        }

        [HttpGet("{username}/follow")]
        public async Task<IActionResult> Get(string userName, [FromQuery] string predicate)
        {
            var profiles = await _followerService.Get(userName, predicate);

            return Ok(profiles);
        }

        #endregion
    }
}