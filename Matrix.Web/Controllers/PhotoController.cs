﻿using Matrix.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Matrix.Web.ApiControllers
{
    [Route("api/photo")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        #region Fields

        private readonly IPhotoService _photoService;

        #endregion

        #region Constructors

        public PhotoController(IPhotoService photoService)
        {
            _photoService = photoService;
        }

        #endregion

        #region Public Methods

        [HttpPost("")]
        public async Task<IActionResult> Create([FromForm] IFormFile file)
        {
            var photo = await _photoService.Create(file);

            return Ok(photo);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var isDeleted = await _photoService.Delete(id);

            return Ok(isDeleted);
        }

        [HttpPost("{id}/setmain")]
        public async Task<IActionResult> SetMain(string id)
        {
            var isSet = await _photoService.SetMain(id);

            return Ok(isSet);
        }

        #endregion
    }
}