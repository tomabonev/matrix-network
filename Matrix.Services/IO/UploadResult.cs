﻿namespace Matrix.Services.Photos
{
    public class UploadResult
    {
        public string PublicId { get; set; }

        public string Url { get; set; }
    }
}
