﻿using System;

namespace Matrix.Services.DTOs
{
    public class CommentDto
    {
        public Guid Id { get; set; }

        public string Body { get; set; }

        public DateTime CreatedAt { get; set; }

        public Guid ActivityId { get; set; }

        public string UserName { get; set; }

        public string DisplayName { get; set; }

        public string Image { get; set; }
    }
}
