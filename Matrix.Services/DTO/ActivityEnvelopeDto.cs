﻿using Matrix.Services.DTOs;
using System.Collections.Generic;

namespace Matrix.Services.DTO
{
    public class ActivityEnvelopeDto
    {
        public List<ActivityDto> Activities { get; set; }

        public int ActivityCount { get; set; }
    }
}
