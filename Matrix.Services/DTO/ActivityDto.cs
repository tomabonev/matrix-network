﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Matrix.Services.DTOs
{
    public class ActivityDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public string City { get; set; }

        public string Venue { get; set; }

        public DateTime? Date { get; set; }

        public bool IsDeleted { get; set; }

        [JsonPropertyName("attendees")]
        public ICollection<AttendeeDto> UserActivities { get; set; }

        public ICollection<CommentDto> Comments { get; set; }
    }
}