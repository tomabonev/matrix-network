﻿using Matrix.Models.Models;
using Matrix.Services.DTOs;
using System.Threading.Tasks;

namespace Matrix.Services.Contracts
{
    /// <summary>
    /// Defines the functionality of a user. 
    /// </summary>
    public interface IUserService
    {
        Task<UserDto> Login(LoginDto model);

        Task<UserDto> Register(RegisterDto model);

        Task<UserDto> GetCurrentUser();
       
        string CreateToken(User user);

        string GetCurrentUserName();
    }
}