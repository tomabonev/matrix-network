﻿using Matrix.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Matrix.Services.Contracts
{
    /// <summary>
    /// Defines the functionality of a photo. 
    /// </summary>
    public interface IPhotoService
    {
        Task<Photo> Create(IFormFile file);

        Task<bool> Delete(string id);

        Task<bool> SetMain(string id);
    }
}