﻿using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.Services.Services
{
    /// <summary>
    /// A User service class that implements IUserService and contains User's authentication proccess.
    /// </summary>
    public class UserService : IUserService
    {
        #region Fields

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;

        #endregion

        #region Constructors

        public UserService(
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        #endregion

        #region Public Methods

        public async Task<UserDto> Login(LoginDto model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                throw new System.WebException(HttpStatusCode.Unauthorized, new { user = "Unauthorized email." });
            }

            var resultSign = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

            if (!resultSign.Succeeded)
            {
                throw new System.WebException(HttpStatusCode.Unauthorized, new { resultSign = "Unauthorized" });
            }

            var userToken = CreateToken(user);

            return new UserDto
            {
                DisplayName = user.DisplayName,
                Token = userToken,
                UserName = user.UserName,
                Image = user.Photos.FirstOrDefault(x => x.IsMain == true)?.Url
            };
        }

        // Photo is not in the register field and implemented during that proccess!
        public async Task<UserDto> Register(RegisterDto model)
        {
            var userExists = await _userManager.FindByEmailAsync(model.Email);

            if (userExists != null)
            {
                throw new System.WebException(HttpStatusCode.BadRequest,
                    new { userExists = "Email already exists!" });
            }

            var user = new User()
            {
                DisplayName = model.DisplayName,
                Email = model.Email,
                UserName = model.UserName
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new System.WebException(HttpStatusCode.BadRequest,
                    new { result = "User creation failed! Please check user details and try again." });
            }

            var userToken = CreateToken(user);

            return new UserDto
            {
                DisplayName = user.DisplayName,
                UserName = user.UserName,
                Token = userToken,
                //Image = user.Photos.FirstOrDefault(x => x.IsMain == true)?.Url
            };
        }

        public async Task<UserDto> GetCurrentUser()
        {
            var user = await _userManager.FindByNameAsync(GetCurrentUserName());

            if (user == null)
            {
                throw new System.WebException(HttpStatusCode.BadRequest,
                    new { user = "Current user cannot be find!" });
            }

            var userToken = CreateToken(user);

            return new UserDto
            {
                DisplayName = user.DisplayName,
                UserName = user.UserName,
                Token = userToken,
                Image = user.Photos.FirstOrDefault(x => x.IsMain == true)?.Url
            };
        }

        public string GetCurrentUserName()
        {
            var username = _httpContextAccessor.HttpContext.User?.Identity.Name;

            return username;
        }

        public string CreateToken(User user)
        {
            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(7),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha512Signature)
                );

            var tokenHandler = new JwtSecurityTokenHandler();

            return tokenHandler.WriteToken(token);
        }

        #endregion
    }
}