﻿using AutoMapper;
using Matrix.DataBase;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTO;
using Matrix.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Matrix.Services.Services
{
    /// <summary>
    /// An Activity service class that implements IActivityService and contains User's activities.
    /// </summary>
    public class ActivityService : IActivityService
    {
        #region Fields

        private readonly IMapper _mapper;
        private readonly MatrixDbContext _context;
        private readonly IUserService _userService;

        #endregion

        #region Constructors

        public ActivityService(MatrixDbContext context, IMapper mapper, IUserService userService)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
        }

        #endregion

        #region Public Methods

        public async Task<ActivityEnvelopeDto> Get(int? limit, int? offset, bool isGoing, bool isHost, DateTime? startDate)
        {
            if (startDate == null)
            {
                startDate = DateTime.UtcNow;
            }

            var queryable = _context.Activities
                .Where(x => x.Date >= startDate)
                .OrderBy(x => x.Date)
                .AsQueryable();

            var currentUserName = _userService.GetCurrentUserName();

            if (isGoing && !isHost)
            {
                queryable = queryable.Where(x => x.UserActivities.Any(a =>
                a.User.UserName == currentUserName));
            }

            if (isHost && !isGoing)
            {
                queryable = queryable.Where(x => x.UserActivities.Any(a =>
                a.User.UserName == currentUserName && a.IsHost == true));
            }

            var activities = await queryable
                .Skip(offset ?? 0)
                .Take(limit ?? 3).ToListAsync();

            var activityToReturn = new ActivityEnvelopeDto
            {
                Activities = _mapper.Map<List<Activity>, List<ActivityDto>>(activities),
                ActivityCount = queryable.Count()
            };

            return activityToReturn;
        }

        public async Task<ActivityDto> Get(Guid activityId)
        {
            if (activityId == null || activityId == Guid.Empty)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { activityId = "Not found" });
            }

            // Lazy Loading
            var activity = await _context.Activities.FindAsync(activityId);

            if (activity == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { activity = "Not found" });
            }

            var activityToReturn = _mapper.Map<Activity, ActivityDto>(activity);

            return activityToReturn;
        }

        public async Task<bool> Attend(Guid id)
        {
            var activity = await _context.Activities.FindAsync(id);

            if (activity == null)
                throw new System.WebException(HttpStatusCode.NotFound, new { Activity = "Cound not find activity" });

            var user = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == _userService.GetCurrentUserName());

            var attendance = await _context.UserActivities
                .SingleOrDefaultAsync(x => x.ActivityId == activity.Id &&
                    x.UserId == user.Id);

            if (attendance != null)
                throw new System.WebException(HttpStatusCode.BadRequest,
                    new { Attendance = "Already attending this activity" });

            attendance = new UserActivity
            {
                Activity = activity,
                User = user,
                IsHost = false,
                DateJoined = DateTime.Now
            };

            _context.UserActivities.Add(attendance);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        public async Task<bool> UnAttend(Guid id)
        {
            var activity = await _context.Activities.FindAsync(id);

            if (activity == null)
                throw new System.WebException(HttpStatusCode.NotFound, new { Activity = "Cound not find activity" });

            var user = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == _userService.GetCurrentUserName());

            var attendance = await _context.UserActivities
                .SingleOrDefaultAsync(x => x.ActivityId == activity.Id &&
                    x.UserId == user.Id);

            if (attendance == null)
                throw new System.WebException(HttpStatusCode.BadRequest,
                    new { Attendance = "Already removed from this activity" });

            if (attendance.IsHost == true)
                throw new System.WebException(HttpStatusCode.BadRequest,
                    new { Attendance = "You cannot remove yourself as host" });

            _context.UserActivities.Remove(attendance);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        public async Task<ActivityDto> Create(ActivityDto activityDto)
        {
            if (activityDto == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { activityDto = "Not found" });
            }

            var activityEntity = new Activity
            {
                Id = activityDto.Id,
                Title = activityDto.Title,
                Description = activityDto.Description,
                Category = activityDto.Category,
                Date = activityDto.Date,
                City = activityDto.City,
                Venue = activityDto.Venue
            };

            if (_context.Activities.Contains(activityEntity))
            {
                throw new System.WebException(HttpStatusCode.BadRequest, new { activity = "Activity with such name already exists." });
            }

            await _context.Activities.AddAsync(activityEntity);

            var user = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == _userService.GetCurrentUserName());

            var attendee = new UserActivity
            {
                User = user,
                Activity = activityEntity,
                IsHost = true,
                DateJoined = DateTime.Now
            };

            _context.UserActivities.Add(attendee);

            var success = await _context.SaveChangesAsync() > 0;

            var activityToReturn = _mapper.Map<Activity, ActivityDto>(activityEntity);

            if (success) return activityToReturn;

            throw new Exception("Problem saving changes");

        }

        public async Task<ActivityDto> Update(ActivityDto activityDto)
        {
            if (activityDto == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { activityDto = "Not found" });
            }

            var activity = await _context.Activities
                .Where(a => a.IsDeleted == false)
                .FirstOrDefaultAsync(a => a.Id == activityDto.Id);

            if (activity == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { activity = "Not found!" });
            }

            var user = await _context.Users.SingleOrDefaultAsync(x =>
                       x.UserName == _userService.GetCurrentUserName());

            var userHost = await _context.UserActivities
                .Where(u => u.IsHost == true)
                .FirstOrDefaultAsync(a => a.ActivityId == activity.Id && a.UserId == user.Id);

            if (userHost == null)
            {
                throw new System.WebException(HttpStatusCode.Forbidden, new { userHost = "You are not the host of the event, so edit is not allowed!" });
            }

            activity.Title = activityDto.Title ?? activity.Title;
            activity.Description = activityDto.Description ?? activity.Description;
            activity.Category = activityDto.Category ?? activity.Category;
            activity.City = activityDto.City ?? activity.City;
            activity.Venue = activityDto.Venue ?? activity.Venue;
            activity.Date = activityDto.Date ?? activity.Date;

            _context.Activities.Update(activity);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return activityDto;

            throw new Exception("Problem saving changes");
        }

        public async Task<bool> Delete(Guid id)
        {
            var activity = await _context.Activities
                .Where(a => a.IsDeleted == false)
                .FirstOrDefaultAsync(a => a.Id == id);

            if (activity == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { activity = "Not found!" });
            }

            var user = await _context.Users.SingleOrDefaultAsync(x =>
           x.UserName == _userService.GetCurrentUserName());

            var userHost = await _context.UserActivities
                .Where(u => u.IsHost == true)
                .FirstOrDefaultAsync(a => a.ActivityId == activity.Id && a.UserId == user.Id);

            if (userHost == null)
            {
                throw new System.WebException(HttpStatusCode.Forbidden, new { userHost = "You are not the host of the event, so delete is not allowed!" });
            }

            activity.IsDeleted = true;

            _context.Activities.Update(activity);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        #endregion
    }
}