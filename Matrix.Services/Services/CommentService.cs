﻿using AutoMapper;
using Matrix.DataBase;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Matrix.Services.Services
{
    /// <summary>
    /// A Comment service class that implements ICommentService and contains User's comments.
    /// </summary>
    public class CommentService : ICommentService
    {
        #region Fields

        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly MatrixDbContext _context;

        #endregion

        #region Constructors

        public CommentService(MatrixDbContext context, IMapper mapper, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Public Methods

        public async Task<CommentDto> Create(CommentDto commentDto)
        {
            var activity = await _context.Activities.FindAsync(commentDto.ActivityId);

            if (activity == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { Activity = "Not found" });
            }

            var username = _httpContextAccessor.HttpContext.User?.Identity.Name;
            var user = await _userManager.FindByNameAsync(username);

            var comment = new Comment
            {
                Author = user,
                Activity = activity,
                Body = commentDto.Body,
                CreatedAt = DateTime.UtcNow
            };

            activity.Comments.Add(comment);

            var success = await _context.SaveChangesAsync() > 0;

            var commentToReturn = _mapper.Map<CommentDto>(comment);

            if (success) return commentToReturn;

            throw new Exception("Problem saving changes");
        }

        #endregion
    }
}