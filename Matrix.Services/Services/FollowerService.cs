﻿using Matrix.DataBase;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Matrix.Services.Services
{
    /// <summary>
    /// An Follower service class that implements IFollowerService and contains User's followers/following.
    /// </summary>
    public class FollowerService : IFollowerService
    {
        #region Fields

        private readonly MatrixDbContext _context;
        private readonly IUserService _userService;
        private readonly IProfileService _profileService;

        #endregion

        #region Constructors

        public FollowerService(MatrixDbContext context, IUserService userService, IProfileService profileService)
        {
            _context = context;
            _userService = userService;
            _profileService = profileService;
        }

        #endregion

        #region Public Methods

        public async Task<bool> Add(string username)
        {
            var observer = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == _userService.GetCurrentUserName());

            var target = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == username);

            if (target == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { User = "Not found" });
            }

            var following = await _context.Followings
                .SingleOrDefaultAsync(x => x.Observer.Id == observer.Id && x.TargetId == target.Id);

            if (following != null)
            {
                throw new System.WebException(HttpStatusCode.BadRequest, new { User = "You are already following this user" });
            }

            if (following == null)
            {
                following = new UserFollowing
                {
                    Observer = observer,
                    Target = target
                };

                _context.Followings.Add(following);
            }

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        public async Task<bool> Delete(string username)
        {
            var observer = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == _userService.GetCurrentUserName());

            var target = await _context.Users.SingleOrDefaultAsync(x =>
                x.UserName == username);

            if (target == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { user = "Not found" });
            }

            var following = await _context.Followings
                .SingleOrDefaultAsync(x => x.Observer.Id == observer.Id && x.TargetId == target.Id);

            if (following == null)
            {
                throw new System.WebException(HttpStatusCode.BadRequest, new { user = "You are not following this user" });
            }

            if (following != null)
            {
                _context.Followings.Remove(following);
            }

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");

        }

        public async Task<List<ProfileDto>> Get(string userName, string predicate)
        {
            var queryable = _context.Followings.AsQueryable();

            var userFollowings = new List<UserFollowing>();

            var profiles = new List<ProfileDto>();

            switch (predicate)
            {
                case "followers":
                    {
                        userFollowings = await queryable.Where(x => x.Target.UserName == userName).ToListAsync();

                        foreach (var follower in userFollowings)
                        {
                            profiles.Add(await _profileService.ReadProfile(follower.Observer.UserName));
                        }
                        break;
                    }

                case "following":
                    {
                        userFollowings = await queryable.Where(x => x.Observer.UserName == userName).ToListAsync();

                        foreach (var follower in userFollowings)
                        {
                            profiles.Add(await _profileService.ReadProfile(follower.Target.UserName));
                        }
                        break;
                    }
            }

            return profiles;
        }

        #endregion
    }
}