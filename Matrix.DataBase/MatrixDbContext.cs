﻿using Matrix.Models;
using Matrix.Models.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Matrix.DataBase
{
    public class MatrixDbContext : IdentityDbContext<User>
    {
        public MatrixDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Activity> Activities { get; set; }

        public DbSet<UserActivity> UserActivities { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<UserFollowing> Followings { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserActivity>(x => x.HasKey(ua =>
             new { ua.UserId, ua.ActivityId }));

            builder.Entity<UserActivity>()
                .HasOne(u => u.User)
                .WithMany(a => a.UserActivities)
                .HasForeignKey(u => u.UserId);

            builder.Entity<UserActivity>()
                .HasOne(a => a.Activity)
                .WithMany(u => u.UserActivities)
                .HasForeignKey(a => a.ActivityId);

            builder.Entity<UserFollowing>(b =>
            {
                b.HasKey(k => new { k.ObserverId, k.TargetId });

                b.HasOne(o => o.Observer)
                .WithMany(f => f.Followings)
                .HasForeignKey(o => o.ObserverId)
                .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(o => o.Target)
                .WithMany(f => f.Followers)
                .HasForeignKey(o => o.TargetId)
                .OnDelete(DeleteBehavior.Restrict);
            });
            
            base.OnModelCreating(builder);
        }
    }
}